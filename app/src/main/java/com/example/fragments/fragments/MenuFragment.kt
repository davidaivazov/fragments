package com.example.fragments.fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.fragments.R

class MenuFragment: Fragment(R.layout.fragment_menu) {

    private lateinit var Btn_cat: Button
    private lateinit var Btn_dog: Button
    private lateinit var Btn_bird: Button


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<TextView>(R.id.menu_favourite).text =
            MenuFragmentArgs.fromBundle(requireArguments()).favouriteText
        Btn_cat = view.findViewById(R.id.btn_cat)
        Btn_dog = view.findViewById(R.id.btn_dog)
        Btn_bird = view.findViewById(R.id.btn_bird)

        val controller = Navigation.findNavController(view)

        Btn_cat.setOnClickListener{
            val action =
               MenuFragmentDirections.actionMenuFragmentToCatFragment()
            controller.navigate(action)
        }

        Btn_dog.setOnClickListener{
            val action =
                MenuFragmentDirections.actionMenuFragmentToDogFragment()
            controller.navigate(action)
        }

        Btn_bird.setOnClickListener{
            val action =
                MenuFragmentDirections.actionMenuFragmentToBirdFragment()
            controller.navigate(action)
        }
    }
}