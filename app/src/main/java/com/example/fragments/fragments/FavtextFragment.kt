package com.example.fragments.fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.fragments.R

class FavtextFragment: Fragment(R.layout.fragment_favtext) {

    private lateinit var Btn_menu : Button
    private lateinit var Favourite_Text_Is: EditText
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Btn_menu = view.findViewById(R.id.menu_btn_text)
        Favourite_Text_Is = view.findViewById(R.id.favourite_text)

        val controller = Navigation.findNavController(view)

        Btn_menu.setOnClickListener{
            var Teqsti = Favourite_Text_Is.text.toString()
                if (Teqsti.isEmpty())
                    Teqsti = "თქვენ არაფერი შეიყვანეთ"
            val favouriteText = Teqsti.toString()
            val action =
                FavtextFragmentDirections.actionFavtextFragmentToMenuFragment(favouriteText)
            controller.navigate(action)
        }
    }
}