package com.example.fragments.fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.fragments.R

class BirdFragment: Fragment(R.layout.fragment_bird) {

    private lateinit var Btn_menu : Button


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Btn_menu = view.findViewById(R.id.menu_btn_bird)
        val controller = Navigation.findNavController(view)

        Btn_menu.setOnClickListener{

            val action =
                BirdFragmentDirections.actionBirdFragmentToMenuFragment(String())
            controller.navigate(action)
        }
    }
}