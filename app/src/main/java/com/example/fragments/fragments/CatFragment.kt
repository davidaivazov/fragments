package com.example.fragments.fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.fragments.R

class CatFragment: Fragment(R.layout.fragment_cat) {

    private lateinit var Btn_menu : Button


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Btn_menu = view.findViewById(R.id.menu_btn_cat)
        val controller = Navigation.findNavController(view)

        Btn_menu.setOnClickListener{

            val action =
                CatFragmentDirections.actionCatFragmentToMenuFragment(String())
                controller.navigate(action)
        }
    }
}